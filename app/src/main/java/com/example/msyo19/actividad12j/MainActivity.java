package com.example.msyo19.actividad12j;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor sensor;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.cordenadas);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensores = sensorManager.getSensorList(Sensor.TYPE_ALL);

        String cadena="";

        if (sensores != null){
            for (int i= 0;i<sensores.size(); i++){
                cadena += sensores.get(i).getName()+ " ==";
            }
        }

        if (sensor == null){
            if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
                sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            }
            else{
                textView.setText("No tiene acelerometro");
            }
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        sensorManager.registerListener((SensorEventListener) this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @SuppressLint("SetTextI18n")

    @Override
    public void onSensorChanged(SensorEvent sensorEvent){
        textView.setText("X: " + sensorEvent.values[0] + "  Y:  " + sensorEvent.values[0] + "  Z:  " + sensorEvent.values[0]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
